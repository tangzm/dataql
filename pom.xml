<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>net.hasor</groupId>
    <artifactId>dataql-parent</artifactId>
    <version>4.3.0-SNAPSHOT</version>
    <packaging>pom</packaging>

    <!-- 配置属性 -->
    <properties>
        <downloadSources>true</downloadSources>
        <downloadJavadocs>true</downloadJavadocs>
        <jacoco.version>0.8.5</jacoco.version>
        <hasor.version>4.2.5</hasor.version>
        <cobble.version>4.4.1</cobble.version>
        <dbvisitor.version>5.1.0</dbvisitor.version>
        <!--<maven.test.skip>true</maven.test.skip>-->
        <!--<skipTests>true</skipTests>-->
    </properties>

    <!-- Project Info -->
    <name>DataQL POM</name>
    <description>the DataBase visitor ORM Tools.</description>
    <url>http://www.dbvisitor.net/</url>
    <inceptionYear>2022</inceptionYear>

    <!-- 授权协议 -->
    <licenses>
        <license>
            <name>The Apache Software License, Version 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
        </license>
    </licenses>

    <!-- SCM -->
    <scm>
        <connection>scm:git:git@git.oschina.net:zycgit/hasor.git</connection>
        <developerConnection>scm:git:git@git.oschina.net:zycgit/hasor.git</developerConnection>
        <url>git@git.oschina.net:zycgit/hasor.git</url>
        <tag>HEAD</tag>
    </scm>

    <developers>
        <developer>
            <name>赵永春(Mr.Zhao)</name>
            <email>zyc@hasor.net</email>
        </developer>
    </developers>

    <modules>
        <module>dataql-engine</module>
        <module>dataql-dataway</module>
        <module>dataql-integration/dataql-sqlproc</module>
        <module>dataql-integration/dataql-functions</module>
        <module>dataql-integration/dataql-maven-plugin</module>
    </modules>

    <!-- 构建描述 -->
    <build>
        <plugins>
            <!-- Compile -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.1</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                    <encoding>UTF8</encoding>
                </configuration>
            </plugin>
            <!-- Source -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>3.2.1</version>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <goals>
                            <goal>jar-no-fork</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <!-- Javadoc -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>3.1.1</version>
                <configuration>
                    <failOnError>false</failOnError>
                    <failOnWarnings>false</failOnWarnings>
                    <doclint>none</doclint>
                    <additionalJOption>--allow-script-in-comments</additionalJOption>
                    <encoding>UTF-8</encoding>
                    <docencoding>UTF-8</docencoding>
                </configuration>
                <executions>
                    <execution>
                        <id>attach-javadocs</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <!-- resources -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <version>3.1.0</version>
                <configuration>
                    <encoding>UTF-8</encoding>
                </configuration>
            </plugin>
            <!-- eclipse 插件 -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-eclipse-plugin</artifactId>
                <version>2.10</version>
                <configuration>
                    <workspace>${basedir}</workspace>
                    <downloadSources>${downloadSources}</downloadSources>
                    <downloadJavadocs>${downloadJavadocs}</downloadJavadocs>
                </configuration>
            </plugin>
            <!-- Test 失败继续 -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.7</version>
                <configuration>
                    <testFailureIgnore>true</testFailureIgnore>
                </configuration>
            </plugin>
            <!-- 覆盖率 -->
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>${jacoco.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>report</id>
                        <phase>test</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <outputDirectory>${project.basedir}/target/jacoco-ut</outputDirectory>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <!-- 使用“-P release”启用下面配置 -->
    <profiles>
        <profile>
            <id>release</id>
            <properties>
                <gpg.executable>gpg2</gpg.executable>
                <gpg.passphrase>fubing220106</gpg.passphrase>
            </properties>
            <build>
                <plugins>
                    <!-- GPG -->
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-gpg-plugin</artifactId>
                        <version>1.6</version>
                        <executions>
                            <execution>
                                <id>sign-artifacts</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.sonatype.plugins</groupId>
                        <artifactId>nexus-staging-maven-plugin</artifactId>
                        <version>1.6.7</version>
                        <extensions>true</extensions>
                        <configuration>
                            <serverId>ossrh</serverId>
                            <nexusUrl>https://oss.sonatype.org/</nexusUrl>
                            <autoReleaseAfterClose>false</autoReleaseAfterClose>
                        </configuration>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-release-plugin</artifactId>
                        <version>2.5.3</version>
                        <configuration>
                            <autoVersionSubmodules>true</autoVersionSubmodules>
                            <useReleaseProfile>false</useReleaseProfile>
                            <releaseProfiles>release</releaseProfiles>
                            <tagNameFormat>v@{project.version}</tagNameFormat>
                            <goals>deploy</goals>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>

    <!-- 公共依赖 -->
    <dependencies>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.13.2</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <!-- 引用依赖 -->
    <dependencyManagement>
        <dependencies>
            <!-- self -->
            <dependency>
                <groupId>net.hasor</groupId>
                <artifactId>hasor-dataql</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>net.hasor</groupId>
                <artifactId>hasor-dataql-fx</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>net.hasor</groupId>
                <artifactId>dataql-sqlproc</artifactId>
                <version>${project.version}</version>
            </dependency>

            <!-- tools -->
            <dependency>
                <groupId>net.hasor</groupId>
                <artifactId>cobble-all</artifactId>
                <version>${cobble.version}</version>
            </dependency>
            <dependency>
                <groupId>net.hasor</groupId>
                <artifactId>dbvisitor</artifactId>
                <version>${dbvisitor.version}</version>
            </dependency>
            <dependency>
                <groupId>net.hasor</groupId>
                <artifactId>dbvisitor-hasor</artifactId>
                <version>${dbvisitor.version}</version>
            </dependency>
            <dependency>
                <groupId>net.hasor</groupId>
                <artifactId>hasor-core</artifactId>
                <version>${hasor.version}</version>
            </dependency>
            <dependency>
                <groupId>net.hasor</groupId>
                <artifactId>hasor-web</artifactId>
                <version>${hasor.version}</version>
            </dependency>
            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>slf4j-api</artifactId>
                <version>1.7.30</version>
            </dependency>
            <dependency>
                <groupId>com.alibaba</groupId>
                <artifactId>fastjson</artifactId>
                <version>1.2.67</version>
            </dependency>
            <dependency>
                <groupId>com.h2database</groupId>
                <artifactId>h2</artifactId>
                <version>1.4.200</version>
            </dependency>
            <dependency>
                <groupId>javax.servlet</groupId>
                <artifactId>javax.servlet-api</artifactId>
                <version>3.1.0</version>
            </dependency>
            <dependency>
                <groupId>javax.servlet.jsp</groupId>
                <artifactId>jsp-api</artifactId>
                <version>2.2</version>
            </dependency>

            <!-- test -->
            <dependency>
                <groupId>junit</groupId>
                <artifactId>junit</artifactId>
                <version>4.12</version>
                <scope>test</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>
</project>
